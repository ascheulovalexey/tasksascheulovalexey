package com.task3;

import java.util.Random;

public class Main {

    public static void main(String[] args) {

        Random random = new Random();
        int[] a = new int[10];

        for (int i = 0; i < a.length; i++) {
            a[i] = random.nextInt(10);
        }

        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }

        int x = random.nextInt(10);
        int counter = 0;

        for (int i = 0; i < a.length; i++) {
            for (int j = i + 1; j < a.length - 1; j++) {
                if (a[i] + a[j] == x) {
                    System.out.print("\nНайдена пара для числа " + x + " : " + a[i] + " + " + a[j]);
                    counter++;
                }
            }
        }

        if (counter == 0){
            System.out.println("\nВ массиве нет пар для числа " + x);
        }

    }
}

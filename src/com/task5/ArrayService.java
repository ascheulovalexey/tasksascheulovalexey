package com.task5;

import java.util.List;
import java.util.Random;

public class ArrayService {

    public static void fillArrayRandomNumber(List column1, List column2, List column3) {
        for (int i = 0; i < 3; i++) {
            Random random = new Random();
            column1.add(random.nextInt(10));
            column2.add(random.nextInt(10));
            column3.add(random.nextInt(10));
        }
    }

    public static void playGame(List column1, List column2, List column3) {

        System.out.println("Текущее поле 3 х 3");
        for (int i = 0; i < 3; i++) {
            System.out.println(column1.get(i) + " " + column2.get(i) + " " + column3.get(i));
        }

        int counter = 0;
        for (int i = 0; i < 3; i++) {
            if (column1.get(i) == column2.get(i) && column2.get(i) == column3.get(i)) {
                System.out.println("\nBingo. Ряд: " + (i + 1));
                System.out.println(column1.get(i) + " " + column2.get(i) + " " + column3.get(i) + " ");
                counter++;
            }
        }

        if (counter == 0) {
            System.out.println("\nВы проиграли");
        }
    }
}

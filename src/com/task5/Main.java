package com.task5;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.task5.ArrayService.fillArrayRandomNumber;
import static com.task5.ArrayService.playGame;

public class Main {

    public static void main(String args[]) {

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("\nНажмите любую кнопку, чтобы сыграть в игру: ");
            String insertKey = scanner.nextLine();
            List<Integer> column1 = new ArrayList<>(3);
            List<Integer> column2 = new ArrayList<>(3);
            List<Integer> column3 = new ArrayList<>(3);
            fillArrayRandomNumber(column1, column2, column3);

            playGame(column1, column2, column3);
        }
    }
}

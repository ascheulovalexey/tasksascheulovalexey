package com.task4;


public class Main {
    public static void main(String[] args) throws InterruptedException {

        Thread trafficLightThread = new Thread(new TrafficLight());
        trafficLightThread.start();

        Thread userThread = new Thread(new UserClass());
        userThread.start();


    }

}


package com.task4;

public class TrafficLight implements Runnable {

    public static volatile TrafficLightColour trafficLightColour = TrafficLightColour.GREEN;

    public static volatile long timer = System.currentTimeMillis();

    public static volatile boolean emergencyMode = false;

    public void run() {

        while (true) {
            if (!emergencyMode) {
                if (trafficLightColour.equals(TrafficLightColour.RED)) {
                    try {
                        turnOnYellowLight();
                        trafficLightColour = TrafficLightColour.RED;
                        Thread.sleep(15000);
                        turnOnYellowLight();
                        trafficLightColour = TrafficLightColour.GREEN;
                        timer = System.currentTimeMillis();
                        UserClass.clickReadLight = false;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                trafficLightColour = TrafficLightColour.YELLOW;
            }
        }
    }

    public void turnOnYellowLight() throws InterruptedException {
        trafficLightColour = TrafficLightColour.YELLOW;
        Thread.sleep(5000);
    }

}
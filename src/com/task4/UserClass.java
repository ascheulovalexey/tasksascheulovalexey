package com.task4;

import java.util.Scanner;

public class UserClass implements Runnable {

    public static volatile boolean clickReadLight = false;

    private int counter = 0;

    @Override
    public void run() {

        while (true) {
            System.out.println("Введите: 1 - узнать сигнал светофора, " +
                    "2 - включить светофор для пешехода " +
                    "3 - включить/отключить аварийный режим");

            Scanner scanner = new Scanner(System.in);
            int insertNumber = scanner.nextInt();

            if (insertNumber == 1) {
                System.out.println(TrafficLight.trafficLightColour);
            } else if (insertNumber == 2) {
                if (clickReadLight == false) {
                    System.out.println("Пешеходный светофор включается...");
                    clickReadLight = true;
                    long currentTimer = (System.currentTimeMillis() - TrafficLight.timer) / 1000;

                    if (currentTimer > 30) {
                        TrafficLight.trafficLightColour = TrafficLightColour.RED;
                    } else {
                        try {
                            System.out.println("Зеленый горит менее 30 секунд." +
                                    "\nПереход на красный начнется через : " + (30 - currentTimer) + " секунд");
                            Thread.sleep((30 - currentTimer) * 1000);
                            TrafficLight.trafficLightColour = TrafficLightColour.RED;
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    System.out.println("Не нажимайте кнопку сразу. Подождите пока загорится зеленый свет");

                }
            } else if (insertNumber == 3) {
                if (counter == 1) {
                    System.out.println("Выключен аварийный режим");
                    TrafficLight.emergencyMode = !TrafficLight.emergencyMode;
                    TrafficLight.trafficLightColour = TrafficLightColour.GREEN;
                    counter--;
                } else {
                    System.out.println("Включен аварийный режим");
                    TrafficLight.emergencyMode = !TrafficLight.emergencyMode;
                    TrafficLight.trafficLightColour = TrafficLightColour.YELLOW;
                    counter++;
                }
            }
        }
    }
}

package com.task1;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> tickets = new ArrayList();
        tickets.add(450306);
        tickets.add(123456);
        tickets.add(832502);
        tickets.add(102111);
        tickets.add(150040);
        tickets.add(936306);

        for (int i = 0; i < tickets.size(); i++) {

            int num = tickets.get(i);

            int first = (num / 100000) % 10;
            int second = (num / 10000) % 10;
            int third = (num / 1000) % 10;
            int fourth = (num / 100) % 10;
            int fifth = (num / 10) % 10;
            int sixth = (num % 10);

            if (first + second + third == fourth + fifth + sixth) {
                System.out.println("Найден счастливый билет в Москве = " + num);
            }

            if (first + third + fifth == second + fourth + sixth) {
                System.out.println("Найден счастливый билет в Петербурге = " + num);
            }
        }
    }
}

package com.task2;

public class Main {

    public static int sum(int sum) {

        System.out.println("Комбинации для суммы: " + sum + " центов");
        int count = 0;
        for (int i = 0; i <= 3; i++) {
            for (int j = 0; j <= 2; j++) {
                for (int k = 0; k <= 1; k++) {
                    int currentSum = i * 50 + j * 20 + k * 10;
                    if (currentSum == sum) {
                        count++;
                        System.out.println(i + " монет 50 центов, " + j + " монет 20 центов, " + k +" монет 10 центов");
                    } else if (currentSum > sum) {
                        break;
                    }
                }
            }
        }
        return count;
    }

    public static void main(String[] args) {
        System.out.println("Общее количество комбинаций: " + sum(70) + "\n");
        System.out.println("Общее количество комбинаций: " + sum(80));
    }
}